// reversewordsCodeJamproblem.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	ifstream inFile;
	ofstream outFile;
	inFile.open("B-small-practice.in");
	outFile.open("results.in");
	int N=0;
	inFile >> N;
	string word1[100];
	int	j = 0;

	cout << "# of Cases: " << N << endl;
	while (!inFile.eof())
	{
		inFile.getline >> word1[0];

	}
	cout << word1[0];
	system("PAUSE");
	return 0;
}


/*

--------------strategy-------------

#1 : open files
#2 : take in first line to get "N" (# of test cases)
#3 : 1 loop should run "N" to get the remaining number of lines
#4 : 1 loop should read each line until \n or endl
#5 : get strings form each line
#6 : reverse each string and print them out.






*/